cryptopals
==========

[cryptopals.org](http://www.cryptopals.org) helps people findß friends to send encrypted email to, so they can practice the discipline and contribute to cultures of privacy. It is a friendly effort to support and expand the base of ordinary email encryptors.

It is a side-project, so I am seeking development assistance to make it serve people better. If you have any ideas, suggestions or pull requests inspired by the basic feature wish-list to-do below, please get involved!

The site is a simple, Django-based fork of (https://github.com/pydanny/cookiecutter-django)[https://github.com/pydanny/cookiecutter-django]. Proficient Django users would make light work of the wish list. I include the README from that project (below) as well as information specific to the cryptopals fork.

I decluttered my commit history by removing it entirely; it is a very simple Django project so shouldn't require much background. I compensate by including an edited `original.log` of the old commit history.

[More info on the project](http://matt.microsplash.org/2014/02/07/cryptopals/)


Cryptopals to-do (wish list)
============================

- Test public key id link to public key server works (i.e. returns page with email and key id on it) or find other solution to ensure public keys are present and functional, as validation before publishing user listing on site.
- 1) Urge and 2) require public key id addition on registration
- Merge user email (`/users/~update/`) and userprofile (`/users/~update/`) forms
- Allow html links in user interests box
- Improve test coverage
- Style tweaks?

- Find a way to unintrusively check that users are responsive to (encrypted) emails from prospective penpals
    - e.g. send encrypted email (including other content) asking if they want to be on the site still and deactivate if no response, where response could be just a designated key word in subject line
- Other improvements and functionality as suggested by you

=======

[cryptopals.org](http://www.cryptopals.org) helps people find a friend to send encrypted email to, so they can practice the discipline. It is a friendly effort to support and expand the base of ordinary email encryptors.

The site is a simple, Django-based fork of (https://github.com/pydanny/cookiecutter-django)[https://github.com/pydanny/cookiecutter-django]. This welcome includes the README from that below as well as information specific to the cryptopals fork.

As a side-project, I am seeking development assistance to make it serve more people and better. If you have any ideas, suggestions or pull requests based on the feature wish-list below, please get involved!

[More info on the project](http://matt.microsplash.org/2014/02/07/cryptopals/)


Original Django-cookiecutter README
===================================

(https://github.com/pydanny/cookiecutter-django)[https://github.com/pydanny/cookiecutter-django]

LICENSE: BSD

Settings
--------

cookiecutter-django relies extensively on environment settings which **will not work with Apache/mod_wsgi setups**. It has been deployed successfully with both Gunicorn/Nginx and even uWSGI/Nginx.

For configuration purposes, the following table maps the cookiecutter-django environment variables to their Django setting:

======================================= =========================== ============================================== ===========================================
Environment Variable                    Django Setting              Development Default                            Production Default
======================================= =========================== ============================================== ===========================================
DJANGO_AWS_ACCESS_KEY_ID                AWS_ACCESS_KEY_ID           n/a                                            raises error
DJANGO_AWS_SECRET_ACCESS_KEY            AWS_SECRET_ACCESS_KEY       n/a                                            raises error
DJANGO_AWS_STORAGE_BUCKET_NAME          AWS_STORAGE_BUCKET_NAME     n/a                                            raises error
DJANGO_CACHES                           CACHES                      locmem                                         memcached
DJANGO_DATABASES                        DATABASES                   See code                                       See code
DJANGO_DEBUG                            DEBUG                       True                                           False
DJANGO_EMAIL_BACKEND                    EMAIL_BACKEND               django.core.mail.backends.console.EmailBackend django.core.mail.backends.smtp.EmailBackend
DJANGO_SECRET_KEY                       SECRET_KEY                  CHANGEME!!!                                    raises error
DJANGO_SECURE_BROWSER_XSS_FILTER        SECURE_BROWSER_XSS_FILTER   n/a                                            True
DJANGO_SECURE_SSL_REDIRECT              SECURE_SSL_REDIRECT         n/a                                            True
DJANGO_SECURE_CONTENT_TYPE_NOSNIFF      SECURE_CONTENT_TYPE_NOSNIFF n/a                                            True
DJANGO_SECURE_FRAME_DENY                SECURE_FRAME_DENY           n/a                                            True
DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS   HSTS_INCLUDE_SUBDOMAINS     n/a                                            True
DJANGO_SESSION_COOKIE_HTTPONLY          SESSION_COOKIE_HTTPONLY     n/a                                            True
DJANGO_SESSION_COOKIE_SECURE            SESSION_COOKIE_SECURE       n/a                                            False
======================================= =========================== ============================================== ===========================================

* TODO: Add vendor-added settings in another table


Developer Installation
-----------------------

For getting this running on your local machine:

1. Set up a virtualenv.
2. Install all the supporting libraries into your virtualenv::

    pip install -r requirements/local.txt

3. Install Grunt Dependencies.

    npm install

4. Run development server. (For browser auto-reload, use Livereload_ plugins.)

    grunt serve

.. _livereload: https://github.com/gruntjs/grunt-contrib-watch#using-live-reload-with-the-browser-extension


Deployment
------------

Run these commands to deploy the project to Heroku:

.. code-block:: bash

    heroku create --buildpack https://github.com/heroku/heroku-buildpack-python
    heroku addons:add heroku-postgresql:dev
    heroku addons:add pgbackups
    heroku addons:add sendgrid:starter
    heroku addons:add memcachier:dev
    heroku pg:promote HEROKU_POSTGRESQL_COLOR
    heroku config:set DJANGO_CONFIGURATION=Production
    heroku config:set DJANGO_SECRET_KEY="****"
    heroku config:set DJANGO_AWS_ACCESS_KEY_ID="***"
    heroku config:set DJANGO_AWS_SECRET_ACCESS_KEY="****"
    heroku config:set DJANGO_AWS_STORAGE_BUCKET_NAME="cryptopal"
    git push heroku master
    heroku run python crypal/manage.py syncdb --noinput --settings=config.settings
    heroku run python crypal/manage.py migrate --settings=config.settings
    heroku run python crypal/manage.py collectstatic --settings=config.settings
