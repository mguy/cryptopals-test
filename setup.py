# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import crypal
version = crypal.__version__

setup(
    name='crypal',
    version=version,
    author='',
    author_email='theabstractarts@gmail.com',
    packages=[
        'crypal',
    ],
    include_package_data=True,
    install_requires=[
        'Django>=1.6.1',
    ],
    zip_safe=False,
    scripts=['crypal/manage.py'],
)